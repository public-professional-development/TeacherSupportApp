package com.inz.teachersupport.personSecurity.personEditProfile;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EditProfileController {

   IServiceProvider serviceProvider;

    @Autowired
    public EditProfileController(IServiceProvider serviceProvider) {
        this.serviceProvider=serviceProvider;
    }
    @GetMapping("/teacherSupportEditProfile")
    String editprofile(Model model) {
        serviceProvider.getIModelService().editProfileModel(model);
        return "teacherSupportEditProfile";
    }
}