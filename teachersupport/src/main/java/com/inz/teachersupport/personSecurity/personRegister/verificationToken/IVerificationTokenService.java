package com.inz.teachersupport.personSecurity.personRegister.verificationToken;

import com.inz.teachersupport.personSecurity.UserSecurityData;

public interface IVerificationTokenService {
    VerificationToken getVerificationToken(String VerificationToken);
    void createVerificationToken(UserSecurityData user, String token,String pass);
    void deleteVerificationToken(VerificationToken token);
    void cleanUserTokens(UserSecurityData user);
}
