package com.inz.teachersupport.personSecurity.personRegister.verificationToken;

import com.inz.teachersupport.personSecurity.UserSecurityData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class VerificationTokenServiceImpl implements IVerificationTokenService {
    @Autowired
    TokenRepo tokenRepo;

    @Override
    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepo.findByToken(VerificationToken);
    }

    @Override
    public void createVerificationToken(UserSecurityData user, String token, String password) {
        VerificationToken myToken = new VerificationToken(token, user, password);
        tokenRepo.save(myToken);
    }

    @Override
    public void deleteVerificationToken(VerificationToken token) {
        tokenRepo.delete(token);
    }

    @Override
    public void cleanUserTokens(UserSecurityData user) {
        List<VerificationToken> allTokens = tokenRepo.findAll();
        Integer user_id = user.getId();
        for (VerificationToken t : allTokens) {
            if (t.getUser().getId().equals(user_id))
                tokenRepo.delete(t);
        }

    }
}
