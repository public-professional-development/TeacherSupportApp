package com.inz.teachersupport.newsP;

import com.inz.teachersupport.person.ServiceResponse;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewsRESTController {

   private IServiceProvider serviceProvider;

    @Autowired
    public NewsRESTController(IServiceProvider serviceProvider) {
      this.serviceProvider=serviceProvider;
    }
    
    @PostMapping("/tshome/delete")
    public ResponseEntity<Object> deleteNews(@RequestBody String newsContent) {
        serviceProvider.getINewsService().deleteNewsByContent(newsContent,serviceProvider);
        ServiceResponse<String> response = new ServiceResponse<String>("success", newsContent);
        return new ResponseEntity<Object>(response, HttpStatus.OK);

    }
}

